FROM node:alpine

WORKDIR /usr/src/app

COPY package.json .

COPY bot.js .

RUN npm install

CMD [ "npm", "start" ]