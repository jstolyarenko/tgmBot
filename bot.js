'use strict'

var TOKEN = '';
process.argv.forEach(function (val, index, array) {
  TOKEN = val;
});

var TelegramBot = require('node-telegram-bot-api');
var bot = new TelegramBot(TOKEN, {polling:true});
var officeMapping = [
	{
		city: "Самара",
		office: "Самара, Чернореченская, 42а",
		schedule: "09:00 - 20:00"
	},
	{
		city: "Тольятти",
		office: "Тольятти, Ленина, 41",
		schedule: "09:00 - 20:00"
	},
];

bot.onText(/\/start/, function onStartText(msg) {	
  bot.sendMessage(msg.chat.id, "Привет! \nили введите /офис <город>, чтобы получить список офисов в вашем городе.\n Например, /офис Самара");
});


bot.onText(/\/офис (.+)/, function onOfficeText(msg, match) {
  var $office = match[1];
  var resArr = officeMapping.filter(function(item) {
  	return (item.city === $office);
  });
  if (resArr.length > 0) {
  	var msgText = "Список офисов в городе " + $office + "\n";
  	for (var i=0; i<resArr.length; i++) {
  		msgText += "-----\n" +
  		"Адрес: " + resArr[i].office + "\n" +
		"График работы: " + resArr[i].schedule	+ "\n";
  	}
  	bot.sendMessage(msg.chat.id, msgText);
  } else {
  	bot.sendMessage(msg.chat.id, $office + "? Думаю это классный город, но нашего офиса в нем пока нет.");
  }
});